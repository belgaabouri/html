.PHONY: update
update:
	d=$$(nix-build ../../icecap -A framework.generatedDocs.external.html --no-out-link) && rm -rf public && cp -rL --no-preserve=owner,mode $$d public
